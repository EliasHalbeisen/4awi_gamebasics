package at.hel.games.basics;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import at.hel.GameBasicsOO.HTLCircle;
import at.hel.GameBasicsOO.HTLOval;
import at.hel.GameBasicsOO.HTLRectangle;
import at.hel.GameBasicsOO.Shootingstar;
import at.hel.GameBasicsOO.Snowflake;
import at.hel.GameBasicsOO.Spaceship;

public class FirstGame extends BasicGame {
	private List<Snowflake> snowflakes;
	private HTLRectangle rectangle;
	private HTLOval oval;
	private HTLCircle circle;
	private Shootingstar shootingstar;
	private Spaceship spaceship;
	

	public FirstGame() {
		super("FirstGame");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics g) throws SlickException {
		for(Snowflake flake : snowflakes) {
			flake.render(g);
		}
		this.rectangle.render(g);
		this.oval.render(g);
		this.circle.render(g);
		this.shootingstar.render(g);
		this.spaceship.render(g);
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		this.snowflakes = new ArrayList<>();

		for(int i = 0; i < 30; i++) {
			snowflakes.add(new Snowflake(0.5, 2));
		}
		for(int i = 0; i < 60; i++) {
			snowflakes.add(new Snowflake(0.25,1));
		}
		for(int i = 0; i < 120; i++) {
			snowflakes.add(new Snowflake(0.125,0));
		}
		
		
		this.rectangle = new HTLRectangle(0, 0, 50, 50, 0);
		this.oval = new HTLOval(0, 350, 50, 30, 0);
		this.circle = new HTLCircle(350, -100, 100, 100);
		this.shootingstar = new Shootingstar(100, 100, 50, 50);
		this.spaceship = new Spaceship(50,50);
	}

	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {
		this.rectangle.move(delta);
		this.oval.move(delta);
		this.circle.move(delta);
		this.shootingstar.move(delta);
		this.spaceship.move(gameContainer,delta);
		for(Snowflake flake : snowflakes) {
			flake.update(delta);
		}
		
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}

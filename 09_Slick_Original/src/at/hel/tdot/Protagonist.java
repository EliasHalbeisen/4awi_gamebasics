package at.hel.tdot;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Protagonist {
	private double x, y;
	private boolean moveleft = false;
	private boolean moveright = false;
	private boolean movedown = false;
	private boolean moveup = false;
	private boolean moveleft2 = false;
	private boolean moveright2 = false;
	private boolean movedown2 = false;
	private boolean moveup2 = false;
	private boolean moveup3 = false;
	private Image image;
	


	public Protagonist(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		try {
			this.image = new Image("testdata/blaster.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void move(GameContainer container, int delta) {
		moveleft = container.getInput().isKeyDown(Input.KEY_A);
		moveright = container.getInput().isKeyDown(Input.KEY_D);
		movedown = container.getInput().isKeyDown(Input.KEY_S);
		moveup = container.getInput().isKeyDown(Input.KEY_W);
		moveleft2 = container.getInput().isKeyDown(Input.KEY_LEFT);
		moveright2 = container.getInput().isKeyDown(Input.KEY_RIGHT);
		movedown2 = container.getInput().isKeyDown(Input.KEY_DOWN);
		moveup2 = container.getInput().isKeyDown(Input.KEY_UP);

		if (moveleft || moveleft2 == true) {
			this.x--;
		}
		if (moveright || moveright2 == true) {
			this.x++;
		}
		if (moveup || moveup2 == true) {
			this.y--;
		}
		if (movedown || movedown2 == true) {
			this.y++;
		}

	}

	public void render(Graphics g) {
		
		this.image.draw((int) this.x, (int)this.y);
		
	}

}



	

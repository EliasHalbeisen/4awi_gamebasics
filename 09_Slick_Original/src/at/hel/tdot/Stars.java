package at.hel.tdot;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Stars {

	private double x, y, speed;
	private int size, diameter;
	
	
	
	public Stars(double speed, int size) {
		setRandomPosition();
		this.speed = speed;
		this.size = size;

		if (this.size == 0) {
			diameter = 4;
		} else if (this.size == 1) {
			diameter = 8;
		} else {
			diameter = 14;
		}
	}
	public void setRandomPosition(){
		Random r = new Random();
		this.x = r.nextInt(1400)+ 1400;
		this.y = r.nextInt(600);
		
	}
	
	public void update(int delta) {
		this.x -= delta *speed;
		
		if(this.x < -10) {
			setRandomPosition();
		}
	}
	public void render(Graphics g) {
		g.setColor(Color.white);
		g.fillOval((int)this.x, (int)this.y, diameter, diameter);
	}
	
}

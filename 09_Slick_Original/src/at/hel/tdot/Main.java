package at.hel.tdot;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Game;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import at.hel.tdot.Stars;

//import at.hel.games.basics.FirstGame;
import at.hel.tdot.Protagonist;

public class Main extends BasicGame {

	private Protagonist protagonist;
	private ArrayList<Obstacle>obstacles;
	private List<Stars> stars;

	public Main(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	
	
	public void init(GameContainer gameContainer) throws SlickException {
		this.obstacles = new ArrayList<>();
		for (int i = 0; i < 25; i++) {
			Random random = new Random();
			int x = random.nextInt(800);
			int y = random.nextInt(700);
			int width = random.nextInt(50);

			Obstacle Obstacle = new Obstacle(x, y, width, 5, 0.15f);
			this.obstacles.add(Obstacle);
			//this.Protagonist.addCollisionPartner(Obstacle);
		}
		this.stars = new ArrayList<>();

		this.protagonist = new Protagonist(50, 50);
		for(int i = 0; i < 10; i++) {
			stars.add(new Stars(0.5, 2));
		}
		
		for(int i = 0; i < 10; i++) {
			stars.add(new Stars(0.25,1));
		}
		for(int i = 0; i < 10; i++) {
			stars.add(new Stars(0.125,0));
		}

		
	}

	
	public void update(GameContainer gameContainer, int millisSinceLastCall) throws SlickException {
		
		for (int i = 0; i < this.obstacles.size(); i++) {
			this.obstacles.get(i).update(millisSinceLastCall);
		}
		this.protagonist.move(gameContainer, millisSinceLastCall);
		for(Stars flake : stars) {
			flake.update(millisSinceLastCall);
		}
	}

	
	@Override
	public void render(GameContainer arg0, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		for (int i = 0; i < this.obstacles.size(); i++) {
			this.obstacles.get(i).render(g);
		}
		
		for(Stars flake : stars) {
			flake.render(g);
		}
		this.protagonist.render(g);
	}

	
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer((Game) new Main("TdoT Game"));
			container.setDisplayMode(1400, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}

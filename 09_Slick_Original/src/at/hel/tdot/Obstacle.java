package at.hel.tdot;

import java.util.Random;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class Obstacle {

	
	private float x, y;
	private float speed;
	private Image image;
	private Shape shape;
	private boolean hasCollision;
	private int runTime;
	
	public Obstacle(float x, float y, float height, float width, float speed) throws SlickException {
		super();
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.runTime = 0;
		image= new Image("testdata/1137550.png");
		this.shape = new Circle(this.x + 22, this.y + 20, this.speed);
		this.hasCollision=false;
	}

	public void update(int millisSinceLastCall) {
		
		this.runTime += millisSinceLastCall;
		if (runTime>10000) {
			this.speed += 0.05;
			runTime = 0;
		}
		
		if(this.x < -150 )
		{
			Random r = new Random();
			this.x = r.nextInt(1400)+ 1400;
			this.y = r.nextInt(600);
			
		}
		else
		{
			this.x = this.x - (this.speed * millisSinceLastCall);
		}
		//this.shape.setX(this.x + 2);
		//this.shape.setY(this.y + 4);
	}

	public void render(Graphics g) {
		
		//image.draw(this.x, this.y);
		image.getScaledCopy(image.getWidth()/4, image.getHeight()/4).draw(this.x,this.y);
		g.draw(this.shape);
	}

	public Shape getShape() {
		return shape;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}

	public boolean isHasCollision() {
		return hasCollision;
	}

	public void setHasCollision(boolean hasCollision) {
		this.hasCollision = hasCollision;
	}
	

	
}

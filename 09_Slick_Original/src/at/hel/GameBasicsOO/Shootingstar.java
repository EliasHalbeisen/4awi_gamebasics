package at.hel.GameBasicsOO;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Shootingstar {

	private double x;
	private double y;
	private int width;
	private int height;
	private static final double SPEED = 0.5;

	public Shootingstar(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	public void move(int delta){
		this.x += (double) delta - 2 *SPEED;
		if(this.x >= 750) {
			this.x = 0;
		}
		
		
		
	}
	public void render(Graphics g) {
		g.setColor(Color.orange);
		g.drawOval((int)this.x,(int)this.y, this.width, this.height);
	}

	
}

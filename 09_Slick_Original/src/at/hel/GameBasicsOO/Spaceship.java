package at.hel.GameBasicsOO;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Spaceship {
	private double x, y;
	private double speed_launch;
	private boolean moveleft = false;
	private boolean moveright = false;
	private boolean movedown = false;
	private boolean moveup = false;
	private boolean moveleft2 = false;
	private boolean moveright2 = false;
	private boolean movedown2 = false;
	private boolean moveup2 = false;
	private boolean launch = false;
	private Image image;
	

	public Spaceship(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		this.speed_launch = 0.1;
		try {
			this.image = new Image("testdata/beer.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void move(GameContainer container, int delta) {
		moveleft = container.getInput().isKeyDown(Input.KEY_A);
		moveright = container.getInput().isKeyDown(Input.KEY_D);
		launch = container.getInput().isKeyDown(Input.KEY_SPACE);
		movedown = container.getInput().isKeyDown(Input.KEY_S);
		moveup = container.getInput().isKeyDown(Input.KEY_W);
		moveleft2 = container.getInput().isKeyDown(Input.KEY_LEFT);
		moveright2 = container.getInput().isKeyDown(Input.KEY_RIGHT);
		movedown2 = container.getInput().isKeyDown(Input.KEY_DOWN);
		moveup2 = container.getInput().isKeyDown(Input.KEY_UP);

		if (moveleft || moveleft2 == true) {
			this.x--;
		}
		if (moveright || moveright2 == true) {
			this.x++;
		}
		if (launch == true) {
			this.speed_launch += ((Math.exp(0.001) - 1));
			this.y -= delta * this.speed_launch;
		}
		if (moveup || moveup2 == true) {
			this.y--;
		}
		if (movedown || movedown2 == true) {
			this.y++;
		}

	}

	public void render(Graphics g) {
		
		this.image.draw((int) this.x, (int)this.y);
		
	}

}

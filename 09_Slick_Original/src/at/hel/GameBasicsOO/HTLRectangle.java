package at.hel.GameBasicsOO;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class HTLRectangle {
	
	private double x;
	private double y;
	private int width;
	private int height;
	private int moveDirection;
	private static final double SPEED = 1;
	
	public HTLRectangle(double x, double y, int width, int height, int moveDirection) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.moveDirection = moveDirection;
	}
	public void move(int delta) {
		if(moveDirection == 0) {
			this.x += (double) delta * SPEED;
			if(x > 750) {
				this.moveDirection = 1;
			}
		}		
		if(moveDirection == 1) {
			this.y += (double) delta * SPEED;
			if(y > 550) {
				this.moveDirection = 2;
			}
		}		
		if(moveDirection == 2) {
			this.x -= (double) delta * SPEED;
			if(x < 0) {
				this.moveDirection = 3;
			}
		}	
		if(moveDirection == 3) {
			this.y -= (double) delta * SPEED;
			if(y < 0) {
				this.moveDirection = 0;
			}
		}		
		
	}	
	public void render(Graphics g) {
		g.setColor(Color.red);
		g.drawRect((int)this.x,(int)this.y, this.width, this.height);	
	}
	
	

}

package at.hel.GameBasicsOO;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Snowflake {

	private double x, y, speed;
	private int size, diameter;

	public Snowflake(double speed, int size) {
		setRandomPosition();
		this.speed = speed;
		this.size = size;

		if (this.size == 0) {
			diameter = 4;
		} else if (this.size == 1) {
			diameter = 8;
		} else {
			diameter = 14;
		}
	}
	public void setRandomPosition(){
		Random r = new Random();
		this.y = r.nextInt(600) - 600;
		this.x = r.nextInt(800);
		
	}
	
	public void update(int delta) {
		this.y += delta *speed;
		
		if(this.y > 600) {
			setRandomPosition();
		}
	}
	public void render(Graphics g) {
		g.setColor(Color.white);
		g.fillOval((int)this.x, (int)this.y, diameter, diameter);
	}
}

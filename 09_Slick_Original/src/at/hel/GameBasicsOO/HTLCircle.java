package at.hel.GameBasicsOO;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class HTLCircle {
	
	private double x;
	private double y;
	private int width;
	private int height;
	private static final double SPEED = 0.5;

	public HTLCircle(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	public void move(int delta){
		this.y += (double) delta - SPEED;
		if(this.y >= 570) {
			this.y = 0;
		}
		
	}
	public void render(Graphics g) {
		g.setColor(Color.cyan);
		g.drawOval((int)this.x,(int)this.y, this.width, this.height);
	}

}